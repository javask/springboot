package com.pancm.service;

import java.util.List;

import com.pancm.pojo.User;
import org.springframework.data.domain.Page;


/**
 * 
 * Title: UserService
 */
public interface UserService {
	
	/**
	 * 新增用户
	 * @param user
	 * @return
	 */
	boolean addUser(User user);
	
	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	boolean updateUser(User user);
	
	
	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	boolean deleteUser(int id);

	/**
	 * 根据名称删除
	 * @param userName
	 * @return
	 */
	boolean deleteUserName(String userName);

	 /**
     * 根据用户名字查询用户信息
     * @param userName
     */
	User findUserByName(String userId);

/*    *//**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @return
     *//*
    Page<User> findByPage(Integer pageNum, Integer pageSize, User user);*/

    /**
     * 查询所有
     * @return
     */
	List<User> findAll();
}
