package com.pancm.pojo;

import java.io.Serializable;

/**
 * 
 * Title: User
 */
public class User implements Serializable {
	 /** 编号 */
	 private int id;

	 /** 姓名 */
	 private String name;

	 /** 性别 */
	 private String sex;

	 /** 住址 */
	 private String address;

	 /** 电话 */
	 private String telephone;

	 /** 年龄 */
	 private int age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
