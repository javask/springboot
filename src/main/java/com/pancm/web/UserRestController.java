package com.pancm.web;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import com.pancm.pojo.User;
import com.pancm.service.UserService;



/**
 * 
* Title: UserRestController
* Description: 
* 用户控制层
 */
@RestController
@RequestMapping(value = "/api")
@Repository
public class UserRestController  {
	@Autowired
    private UserService userService;

	@ApiOperation( value = "新增用户" )
	@RequestMapping(value = "/user", method = RequestMethod.POST)
    public boolean addUser(@RequestBody User user)
    {
    	System.out.println("开始新增...");
        return userService.addUser(user);
    }

    @ApiOperation( value = "更新")
	@RequestMapping(value = "/user", method = RequestMethod.PUT)
    public boolean updateUser(@RequestBody User user) {
    	System.out.println("开始更新...");
        return userService.updateUser(user);
    }
	@ApiOperation( value = "删除")
	@RequestMapping(value = "/user", method = RequestMethod.DELETE)
    public boolean delete(@RequestParam(value = "userId", required = true) int userId) {
    	System.out.println("开始删除...");
        return userService.deleteUser(userId);
    }

    @ApiOperation( value = "根据名字删除",notes = "名字删除userName")
	@RequestMapping(value = "/name",method = RequestMethod.DELETE)
    public boolean delete(@RequestParam(value = "userName",required = true) String userName){
        System.out.println("开始根据名字删除...");
        return userService.deleteUserName( userName );
    }

    @ApiOperation( value = "用户查询",notes = "查询userId")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public User findByUserName(@RequestParam(value = "userId", required = true) String userId) {
    	System.out.println("开始查询...");
        return userService.findUserByName(userId);
    }

    @RequestMapping(value = "/userAll", method = RequestMethod.GET)
    public List<User> findByUserAge() {
    	System.out.println("开始查询所有数据...");
        return userService.findAll();
    }
}
