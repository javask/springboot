package com.pancm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.pancm.dao.UserDao;
import com.pancm.pojo.User;
import com.pancm.service.UserService;

/**
 * 
* Title: UserServiceImpl
* Description: 
* 用户操作实现类
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;


    @Override
    public boolean addUser(User user) {
        boolean flag = false;
        try {
            userDao.addUser( user );
            flag = true;
        } catch (Exception e) {
            System.out.println( "新增失败!" );
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean updateUser(User user) {
        boolean flag = false;
        try {
            userDao.updateUser( user );
            flag = true;
        } catch (Exception e) {
            System.out.println( "修改失败!" );
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean deleteUser(int id) {
        boolean flag = false;
        try {
            userDao.deleteUser( id );
            flag = true;
        } catch (Exception e) {
            System.out.println( "删除失败!" );
            e.printStackTrace();
        }
        return flag;
    }


    @Override
    public boolean deleteUserName(String userName) {
        boolean flag = false;
        try {
            userDao.deleteUserName( userName );
            flag = true;
        } catch (Exception e) {
            System.out.println( "根据用户名删除失败!" );
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public User findUserByName(String userId) {
        return userDao.findByName( userId );
    }


    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }
/*


/**
     * 分页查询
     *
     * @param pageNum
     * @param pageSize
     * @return
     *//*

    @Override
    public Page<User> findByPage(Integer pageNum, Integer pageSize, User user) {
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (pageSize == null || pageSize == 0) {
            pageSize = 2;
        }
               // 第一种方式模糊查询并且分页
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith())//模糊查询匹配开头，即{username}%
//                .withIgnorePaths("sex")
//                .withIgnorePaths("id");
//        Example<User> example = Example.of(User, matcher);
        return null;
    }
*/

}
